# aws_lambda_function pour contrôler le climatiseur
# aws_iam_role pour le rôle de Lambda
resource "aws_iam_role" "lambda_execution_role" {
  name = "lambda_execution_role"
  
  assume_role_policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "Service": "lambda.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
      }
    ]
  })

  # Ajoutez ici d'autres politiques ou autorisations nécessaires pour votre fonction Lambda
}

# aws_lambda_function pour contrôler le climatiseur
resource "aws_lambda_function" "ac_control_lambda" {
  filename      = "files/empty_package.zip"  # Assurez-vous que le chemin du fichier zip est correct
  function_name = "ac_control_lambda"
  handler       = "ac_control_lambda.lambda_handler"
  runtime       = "python3.7"
  role          = aws_iam_role.lambda_execution_role.arn  # Utilisez l'ARN du rôle que vous avez créé
}


# aws_cloudwatch_event_rule pour une action planifiée toutes les minutes
resource "aws_cloudwatch_event_rule" "every_one_minute" {
  name                = "EveryOneMinute"
  schedule_expression = "rate(1 minute)"
}

# aws_cloudwatch_event_target pour lier l'événement planifié et la fonction lambda
resource "aws_cloudwatch_event_target" "lambda_target" {
  rule      = aws_cloudwatch_event_rule.every_one_minute.name
  target_id = "lambda"
  arn       = aws_lambda_function.ac_control_lambda.arn
}

# aws_lambda_permission pour autoriser CloudWatch (événement) à appeler la fonction lambda
resource "aws_lambda_permission" "allow_execution" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ac_control_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.every_one_minute.arn
}
