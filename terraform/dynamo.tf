# aws_dynamodb_table
resource "aws_dynamodb_table" "Temperature" {
  name           = "Temperature"
  billing_mode   = "PAY_PER_REQUEST"  # Ou "PROVISIONED" selon votre besoin

  # Configuration de la clé principale
  hash_key = "id"
  attribute {
    name = "id"
    type = "S"  # Chaîne de caractères
  }

  # Configuration supplémentaire de la table DynamoDB si nécessaire
  tags = {
    Name = "TemperatureTable"
  }
}
